package cz.trixi.download;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;

import static cz.trixi.App.*;

public class Downloader {

    public static String downloadFromUrl(String url) {
        try {
            FileSystem fileSystem = FileSystems.getDefault();
            BufferedInputStream in = new BufferedInputStream(new URL(url).openStream());
            Files.createDirectories(fileSystem.getPath(DIRECTORY_NAME));
            FileOutputStream fileOutputStream = new FileOutputStream(DIRECTORY_NAME + FILE_NAME);
            byte[] dataBuffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                fileOutputStream.write(dataBuffer, 0, bytesRead);
            }
            System.out.println("File from url: " + url + " was downloaded to " + DIRECTORY_NAME + " as " + FILE_NAME);
            System.out.println();
        } catch (IOException ex) {
            ex.printStackTrace();
        }


        return DIRECTORY_NAME + FILE_NAME;
    }
}
