package cz.trixi;

import cz.trixi.db.Database;
import cz.trixi.download.Downloader;
import cz.trixi.reader.XMLReader;
import cz.trixi.unzip.Unzipper;

import java.sql.SQLException;

public class App {

    // configure directory where downloaded and unzipped files will be saved
    public static final String DIRECTORY_NAME = "files/";
    // configure downloading link
    public static final String FILE_URL = "https://vdp.cuzk.cz/vymenny_format/soucasna/20200930_OB_573060_UZSZ.xml.zip";
    // configure downloaded file name
    public static final String FILE_NAME = "downloaded.zip";

    public static void main(String[] args) {
        try {
            // Connecting to DB
            Database.connect();
            // Creating tables for DB
            Database.createDB();
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }

        // downloading files from link
        String downloadedFile = Downloader.downloadFromUrl(FILE_URL);

        // unzipping zip files
        String unzippedFileName = Unzipper.unzip(downloadedFile);

        // reading xml file and saving the part needed to DB
        XMLReader reader = new XMLReader(unzippedFileName);
        reader.readXMLAndWriteToDB();
    }
}