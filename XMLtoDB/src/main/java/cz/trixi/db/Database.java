package cz.trixi.db;

import java.sql.*;

public class Database {

    public static Connection conn;
    public static Statement statmt;

    // configure db name
    private final static String dbName = "Db.db";

    public static void connect() throws ClassNotFoundException, SQLException {
        conn = null;
        Class.forName("org.sqlite.JDBC");
        conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);

        System.out.println("Database connected.\n");
    }

    public static void createDB() throws SQLException {

        statmt = conn.createStatement();
        statmt.execute("DROP TABLE IF EXISTS 'municipality'");
        statmt.execute("DROP TABLE IF EXISTS 'municipality_part'");
        statmt.execute("CREATE TABLE 'municipality' ('municipality_code' INTEGER PRIMARY KEY,'name' text NOT NULL);");
        statmt.execute("CREATE TABLE 'municipality_part' ('municipality_part_code' INTEGER PRIMARY KEY,'name' text NOT NULL,'municipality_code' INTEGER NOT NULL,FOREIGN KEY (municipality_code) REFERENCES municipality(municipality_code));");

        System.out.println("Database is created.\n");
    }

    public static void InsertInDBMunicipality(String code, String name) throws SQLException {

        String toInsert = "INSERT INTO 'municipality' ('municipality_code','name') VALUES (" + code + ",\'" + name + "\'" + ");";
        statmt.execute(toInsert);
    }

    public static void InsertInDBMunicipalityPart(String code, String name, String municipalityCode) throws SQLException {

        String toInsert = "INSERT INTO 'municipality_part' ('municipality_part_code','name','municipality_code' ) VALUES (" + code + ",\'" + name + "\'," + municipalityCode + ");";
        statmt.execute(toInsert);
    }
}
