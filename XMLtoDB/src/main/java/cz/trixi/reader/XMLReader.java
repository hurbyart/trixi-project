package cz.trixi.reader;

import cz.trixi.db.Database;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.sql.SQLException;

public class XMLReader {

    File xmlFile;
    Document doc;

    public XMLReader(String xmlPath) {
        this.xmlFile = new File(xmlPath);

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void readXMLAndWriteToDB() {
        getAndWriteToDBMunicipalityInfo();
        getAndWriteToDBMunicipalityPartsInfo();
        try {
            Database.conn.close();
        } catch (SQLException ex){
            ex.printStackTrace();
        }
        System.out.println();
    }

    private void getAndWriteToDBMunicipalityInfo() {
        NodeList nList = doc.getElementsByTagName("vf:Obec");
        for (int temp = 0; temp < nList.getLength(); temp++) {

            Node nNode = nList.item(temp);


            if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) nNode;

                String code = eElement.getElementsByTagName("obi:Kod").item(0).getTextContent();
                String name = eElement.getElementsByTagName("obi:Nazev").item(0).getTextContent();

                try {
                    Database.InsertInDBMunicipality(code, name);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

            }
        }
        System.out.println(nList.getLength() + " elements added to DB.");
    }

    private void getAndWriteToDBMunicipalityPartsInfo() {
        NodeList nList = doc.getElementsByTagName("vf:CastObce");
        for (int temp = 0; temp < nList.getLength(); temp++) {

            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) nNode;
                String code = eElement.getElementsByTagName("coi:Kod").item(0).getTextContent();
                String name = eElement.getElementsByTagName("coi:Nazev").item(0).getTextContent();

                NodeList nl = eElement.getElementsByTagName("coi:Obec");
                Element el = (Element) nl.item(0);
                String municipalityCode = el.getElementsByTagName("obi:Kod").item(0).getTextContent();

                try {
                    Database.InsertInDBMunicipalityPart(code, name, municipalityCode);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

            }
        }
        System.out.println(nList.getLength() + " elements added to DB.");
    }

}