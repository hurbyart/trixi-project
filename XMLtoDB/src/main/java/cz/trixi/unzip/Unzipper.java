package cz.trixi.unzip;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static cz.trixi.App.DIRECTORY_NAME;

public class Unzipper {

    public static String unzip(String fileName) {
        String unzippedName = null;

        try (ZipFile file = new ZipFile(fileName)) {
            FileSystem fileSystem = FileSystems.getDefault();
            Enumeration<? extends ZipEntry> entries = file.entries();

            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (entry.isDirectory()) {
                    System.out.println("Creating Directory:" + entry.getName());
                    Files.createDirectories(fileSystem.getPath(DIRECTORY_NAME + entry.getName()));
                } else {
                    InputStream is = file.getInputStream(entry);
                    BufferedInputStream bis = new BufferedInputStream(is);
                    String uncompressedFileName = entry.getName();
                    Path uncompressedFilePath = fileSystem.getPath(DIRECTORY_NAME + uncompressedFileName);
                    Files.deleteIfExists(uncompressedFilePath);
                    Files.createFile(uncompressedFilePath);
                    FileOutputStream fileOutput = new FileOutputStream(DIRECTORY_NAME + uncompressedFileName);
                    while (bis.available() > 0) {
                        fileOutput.write(bis.read());
                    }
                    fileOutput.close();
                    unzippedName = entry.getName();
                }
            }
            System.out.println("Zip file " + fileName + " was unzipped to " + DIRECTORY_NAME);
            System.out.println("Name of unzipped file is " + unzippedName);
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return DIRECTORY_NAME + unzippedName;
    }
}
